import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './App';

configure({ adapter: new Adapter() });

export interface Contact {
  name: string;
  email: string;
  id: number;
}

it('App component renders without crashing', () => {
  const wrapper = shallow(
    <Router>
      <App />
    </Router>
  );
  expect(wrapper).toMatchSnapshot();

});

