import * as React from 'react';
import { Switch, Route, withRouter, RouteComponentProps, Link, Redirect } from 'react-router-dom';
import styled from 'styled-components';
import ContactList from './components/ContactList';
import CreateContact from './components/contact/Create';
import EditContact from './components/contact/Edit';
import ViewContact from './components/contact/View';

import { AppBar, Toolbar, Typography, Button }  from '@material-ui/core';

import { ApolloProvider } from '@apollo/react-hooks';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createHttpLink } from 'apollo-link-http';
import ApolloClient from 'apollo-client';
import { DefaultOptions } from 'apollo-boost';

import './App.css';

const link = createHttpLink({ uri: "http://localhost:3001/contacts" });

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
}

export const client = new ApolloClient({
  link,
  defaultOptions,
  cache: new InMemoryCache()
});

const StyledLink = styled(Link)`
    text-decoration: none;
    color: #fff;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

class App extends React.Component<RouteComponentProps<any>> {
  public render() {
    return (
      <ApolloProvider client={client}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className="title">
              <Button variant="contained" color="primary">
                <StyledLink to={'/contacts'}>Home</StyledLink>
              </Button>
            </Typography>
              <Button variant="contained" color="secondary">
                <StyledLink to={'/contact/create'}>Create Contact</StyledLink>
              </Button>
          </Toolbar>
        </AppBar>
        <Switch>
            <Route exact path="/">
              <Redirect to="/contacts" />
            </Route>
            <Route path={'/contacts'} exact component={ContactList} />
            <Route path={'/contact/create'} component={CreateContact} />
            <Route path={'/contact/edit/:id'} component={EditContact} />
            <Route path={'/contact/view/:id'} component={ViewContact} />
        </Switch>
      </ApolloProvider>
    );
  }
}

export default withRouter(App);
