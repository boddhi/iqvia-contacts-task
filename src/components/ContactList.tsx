import React, { Component } from "react";
import { withRouter, RouteComponentProps } from 'react-router-dom';
import Moment from 'react-moment';

import { 
  Paper,
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Button,
  ButtonGroup
} from '@material-ui/core';

import { client } from '../App';
import { DELETE_CONTACT } from '../graphql/mutations';
import { ALL_CONTACTS } from '../graphql/queries';

Moment.globalFormat = 'D-MM-YYYY HH:mm';
interface IContact {
  name: string;
  email: string;
  created: string;
  updated: string;
  id: string;
}
interface IState {
  contacts: IContact[]
}

class ContactList extends Component<RouteComponentProps, IState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = { 
            contacts: []
        }
    }

    public componentDidMount(): void {
      this.getContacts();
    }

    getContacts = () => {
      client.query({
        query: ALL_CONTACTS
      }).then(result => {
        this.setState({
          contacts: result.data.contacts,
        });
      })
    }

    private deleteContact(e: React.SyntheticEvent, id: string) {
      e.stopPropagation();
      client.mutate({
        mutation: DELETE_CONTACT,
        variables: {
          id
        }
      }).then(result => {
        if (result?.data?.deleteContact) {
          this.setState({
            contacts: this.state.contacts.filter(item => item.id !== id)
          })
        }
        this.props.history.push('/contacts');
      });
    }

    private editContact(e: React.SyntheticEvent, id: string) {
      e.stopPropagation();
      this.props.history.push(`/contact/edit/${id}`);
    }

    private handleRowClick(e: React.SyntheticEvent, id: string) {
      e.preventDefault();
      this.props.history.push(`/contact/view/${id}`);
    }

    public render() {
      const { contacts } = this.state;
      return (         
        <Paper style={{ margin: 16, padding: 16 }} elevation={0}>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell style={{width: '20%'}}>Name</TableCell>
                  <TableCell style={{width: '20%'}}>Email</TableCell>
                  <TableCell style={{width: '20%'}}>Created</TableCell>
                  <TableCell style={{width: '20%'}}>Updated</TableCell>
                  <TableCell style={{ width: '20%', textAlign: 'right' }}>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {contacts?.map(contact => (
                  <TableRow key={contact.id} 
                    hover 
                    style={{ cursor: 'pointer'}} 
                    onClick={(e) => this.handleRowClick(e, contact.id)}
                  >
                    <TableCell>{contact.name}</TableCell>
                    <TableCell>{contact.email}</TableCell>
                    <TableCell><Moment>{contact.created}</Moment></TableCell>
                    <TableCell>{contact.updated ? <Moment>{contact.updated}</Moment> : 'Never'}</TableCell>
                    <TableCell align="right">
                      <ButtonGroup variant="text" color="primary">
                        <Button onClick={(e) => this.editContact(e, contact.id)}>Edit</Button>
                        <Button onClick={(e) => this.deleteContact(e, contact.id)}>Delete</Button>
                      </ButtonGroup>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      )
    }
}

export default withRouter(ContactList);
