import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import {
    TextField,
    Paper,
    Button,
    Grid
} from '@material-ui/core';

import { client } from '../../App';
import { CREATE_CONTACT } from '../../graphql/mutations';

export interface IValues {
    name: string,
    email: string,
}

export interface IFormState {
    [key: string]: any;
    values: IValues[];
    submitSuccess: boolean;
}

class CreateContact extends React.Component<RouteComponentProps, IFormState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            name: '',
            email: '',
            values: [],
            submitSuccess: false,
        }
    }

    private processFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();
        const formData = {
            name: this.state.name,
            email: this.state.email,
        }
        client.mutate({
            mutation: CREATE_CONTACT,
            variables: {
                name: this.state.name,
                email: this.state.email,
                created: new Date()
            }
          }).then(() => {
            this.setState({ submitSuccess: true, values: [...this.state.values, formData] });
            this.props.history.push('/contacts');
          })

    }

    private handleChange = (e: any)=> {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    public render() {
        const { submitSuccess } = this.state;
        return (
            <Paper style={{ margin: 16, padding: 16 }} elevation={0}>
                
                <h2>New Contact</h2>
                <form id={"create-contact-form"} onSubmit={this.processFormSubmission} noValidate={false}>
                <Grid container>
                <Grid xs={7} md={5} item style={{ paddingRight: 16 }}>
                    <TextField
                        id="name"
                        label="Name"
                        required
                        onChange={(e) => this.handleChange(e)}
                        name="name"
                        fullWidth
                    />
                    </Grid>
                    <Grid xs={7} md={5} item style={{ paddingRight: 16 }}>
                    <TextField
                        id="email"
                        label="Email"
                        required
                        onChange={(e) => this.handleChange(e)}
                        name="email"
                        fullWidth
                    />
                    </Grid>
                    <Grid xs={3} md={2} item>
                    <Button variant="contained" size="large" color="primary" type="submit">
                        Save
                    </Button>
                    </Grid>

                    {submitSuccess && (
                    <div className="alert alert-info" role="alert">
                        The contact was successfully added!
                        </div>
                    )}
                    </Grid>
                </form>
                
            </Paper>
        )
    }
}

export default withRouter(CreateContact)
