import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import {
    TextField,
    Paper,
    Button,
    Grid
} from '@material-ui/core';

import { client } from '../../App';
import { SINGLE_CONTACT } from '../../graphql/queries';
import { EDIT_CONTACT } from '../../graphql/mutations';

export interface IValues {
    [key: string]: any;
}

export interface IFormState {
    id: number,
    contact: any;
    values: IValues[];
}

class EditContact extends React.Component<RouteComponentProps<any>, IFormState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            contact: {},
            values: [],
        }
    }

    public componentDidMount(): void {
        this.getSingleContact(this.state.id);
    }

    getSingleContact = (id: number) => {
        client.query({
            query: SINGLE_CONTACT,
            variables: {
                id
            }
        }).then(result => {
            this.setState({ contact: result.data.contact });
        })
    }

    private processFormSubmission = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();
        client.mutate({
            mutation: EDIT_CONTACT,
            variables: {
              name: this.state.contact.name,
              email: this.state.contact.email,
              id: this.state.id,
              updated: new Date()
            }
        }).then(() => {
            this.props.history.push('/contacts');
        })
    }


    private setValues = (values: IValues) => {        
        this.setState({ 
            values: { ...this.state.values, ...values },
            contact: { ...this.state.contact, ...values }
        });
    }

    private handleChange = (e: any)=> {
        e.preventDefault();
        this.setValues({ 
            [e.target.id]: e.target.value
        })
    }

    public render() {
        const { contact } = this.state;
        return (

            <Paper style={{ margin: 16, padding: 16 }} elevation={0}>
                    <h2>Edit Contact</h2>

                    <form id={"create-contact-form"} onSubmit={this.processFormSubmission} noValidate={true}>
                    <Grid container>
                        <Grid xs={7} md={5} item style={{ paddingRight: 16 }}>
                        <TextField
                            id="name"
                            label="Name"
                            required
                            value={contact.name ?? ''}
                            onChange={(e) => this.handleChange(e)}
                            name="name"
                            fullWidth
                        />
                    </Grid>
                    <Grid xs={7} md={5} item style={{ paddingRight: 16 }}>
                        <TextField
                            id="email"
                            label="Email"
                            required
                            value={contact.email ?? ''}
                            onChange={(e) => this.handleChange(e)}
                            name="email"
                            fullWidth
                        />
                        </Grid>
                        <Grid xs={3} md={2} item>
                        <Button variant="contained" size="large" color="primary" type="submit">
                            Save
                        </Button>
                        </Grid>
                    </Grid>
                    </form>
                    </Paper>
        )
    }
}

export default withRouter(EditContact)
