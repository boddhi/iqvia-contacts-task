import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Moment from 'react-moment';

import {
    Card,
    CardContent,
    Typography,
    Paper,
} from '@material-ui/core';

import { client } from '../../App';
import { SINGLE_CONTACT } from '../../graphql/queries';

export interface IFormState {
    id: string,
    contact: any;
}

Moment.globalFormat = 'D-MM-YYYY HH:mm';

class ViewContact extends React.Component<RouteComponentProps<any>, IFormState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            contact: {},
        }
    }

    public componentDidMount(): void {
        this.getSingleContact(this.state.id);
    }

    getSingleContact = (id: string) => {
        client.query({
            query: SINGLE_CONTACT,
            variables: {
                id
            }
        }).then(result => {
            this.setState({ contact: result.data.contact });
        })
      }

    public render() {
        return (
            <Paper style={{ margin: 16, padding: 16 }} elevation={0}>
                <h2>Contact Details</h2>
                {this.state.contact &&
                    <Card elevation={1}>
                        <CardContent>
                            <Typography variant="subtitle1">
                                Name: { this.state.contact.name }
                            </Typography>
                            <Typography variant="subtitle1">
                                E-mail: { this.state.contact.email }
                            </Typography>
                            <Typography variant="subtitle1">
                                Created: <Moment>{ this.state.contact.created }</Moment>
                            </Typography>
                            <Typography variant="subtitle1">
                                Updated: { this.state.contact.updated ? <Moment>{this.state.contact.updated}</Moment> : 'Never'}
                            </Typography>
                        </CardContent>
                    </Card>
                }
            </Paper>
        )
    }
}

export default withRouter(ViewContact);
