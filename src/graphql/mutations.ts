import { gql } from 'apollo-boost';

export const DELETE_CONTACT = gql`
  mutation DeleteContact($id: ID) {
    deleteContact (id: $id)
  }
`;

export const EDIT_CONTACT = gql`
  mutation EditContact($id: ID, $name: String, $email: String, $updated: Date) {
    updateContact(
        contact:{
          name: $name
          email: $email
          id: $id
          updated: $updated
        }
      ){
        name
        email
        id
        updated
      }
  }
`;

export const CREATE_CONTACT = gql`
  mutation CreateContact($name: String, $email: String, $created: Date) {
    addContact(
        contact:{
          name: $name
          email: $email
          created: $created
        }
      ){
        name
        email
        id
        updated
      }
  }
`;