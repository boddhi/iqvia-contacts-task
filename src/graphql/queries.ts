import { gql } from 'apollo-boost';

const CONTACT_FRAGMENT = gql`
  fragment contactFragment on Contact {
    name
    email
    id
    created
    updated
  }
`;

export const ALL_CONTACTS = gql`
  query AllContactsData {
    contacts {
        ...contactFragment
    }
  }
  ${CONTACT_FRAGMENT}
`;

export const SINGLE_CONTACT = gql`
  query SingleContactData($id: ID) {
    contact(id: $id) {
        ...contactFragment
    }
  }
  ${CONTACT_FRAGMENT}
`;