import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { MockedProvider } from '@apollo/react-testing';
import wait from 'waait';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';

import { ALL_CONTACTS } from '../graphql/queries';
import ContactList from '../components/ContactList';

it('ContactList renders', async () => {

  configure({ adapter: new Adapter() });

  const mocks = [
    {
      request: {
        query: ALL_CONTACTS
      },
      result: {
        data: {
          contacts: [{
            id: 'gGNfmDWXIS1C_ehaKFwHF',
            name: 'John Smith',
            email: 'john@smith.com',
            created: '2020-03-15T16:22:34.686Z'
          }],
        }
      }
    }
  ];

  const wrapper = shallow(
    <MockedProvider mocks={mocks}>
      <Router>
        <ContactList />
      </Router>
    </MockedProvider>
  );
  
  await wait(1000);

  expect (wrapper.contains('John Smith'));

});