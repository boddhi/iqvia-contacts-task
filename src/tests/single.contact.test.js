import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { MockedProvider } from '@apollo/react-testing';
import wait from 'waait';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';

import { SINGLE_CONTACT } from '../graphql/queries';
import ViewContact from '../components/contact/View';

it('ViewContact renders', async () => {

  configure({ adapter: new Adapter() });

  const mocks = [
    {
      request: {
        query: SINGLE_CONTACT,
        variables: {
            id: 'gGNfmDWXIS1C_ehaKFwHF'
        }
      },
      result: {
        data: {
          contact: {
            id: 'gGNfmDWXIS1C_ehaKFwHF',
            name: 'John Smith',
            email: 'john@smith.com',
            created: '2020-03-15T16:22:34.686Z',
            updated: '2020-03-15T16:22:34.686Z'
          },
        }
      }
    }
  ];

  const wrapper = shallow(
    <MockedProvider mocks={mocks}>
      <Router>
        <ViewContact />
      </Router>
    </MockedProvider>
  );
  
  await wait(1000);

  expect (wrapper.contains('John Smith'));

});